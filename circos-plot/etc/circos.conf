# Parse basic configuration of the image
<image>
<<include etc/image.conf>>
file*   = Bipartite-network.png
radius* = 2100p
</image>


# Parse information regarding ideogram parameters
<<include ideogram.conf>>

# Chromosome parameters
#chromosomes                 = -GO1
chromosomes_display_default = yes
#chromosomes                 = GO2,GO3,GO4,GO5,GO6,GO7,GO8,GO9,GO10,GO11,GO12,GO13,GO14,GO15
#chromosomes_color   = GO2=vvdred,GO3=vvdblue
chromosomes_radius  = GO2:1.15r


# Parse info regarding ticks - names of genes
<<include ticks.conf>>


# Path to karyotype (Gene-groups GOterm-groups)
karyotype = data/karyotype.txt

# Configure parameters of plotting (label-genes-names and lines/scatter/histogram)
<plots>

# Plot label-name genes
<plot>
type       = text
file       = data/segments.label.txt
color      = black
label_font = bold
label_size = 20
r0         = 1.13r
r1         = 1.33r
rpadding   = 10p
</plot>

# Plot intra-degree histogram
<plot>
type      = histogram
file      = data/measure.intra-degree.txt

r1        = 1.13r
r0        = 1.04r
max       = 6
min       = 0

stroke_type = outline
thickness   = 4
color       = vvdgreen
extend_bin  = yes

# Colorize using rules
<rules>
<rule>
condition  = 1
fill_color = vdgreen
</rule>
</rules>
</plot>

# Plot heatmap mean intra-degree
<plot>
type      = heatmap
file      = data/measure.heatmap.mean-intradegree.goterms.txt

r1        = 1.03r
r0        = 1.01r
max       = 3
min       = 0

color       = greens-3

</plot>


# Plot heatmap mean AP-MS
<plot>
type      = heatmap
file      = data/measure.heatmap.mean-AP-MS.goterms.txt

r1        = 0.99r
r0        = 0.97r
max       = 50
min       = 0

color       = reds-3

</plot>

# Plot mean AP-MS histogram
<plot>
type      = histogram
file      = data/measure.mean-AP-MS-degree.txt

orientation = in
r1        = 0.96r
r0        = 0.87r
max       = 240
min       = 0

stroke_type = outline
thickness   = 4
color       = vvdred
extend_bin  = yes

# Colorize using rules
<rules>
<rule>
condition  = 1
fill_color = vdred
</rule>
</rules>
</plot>

</plots>

# Define parameters for links (connectogram)
<links>
<link>
file          = data/links.txt
ribbon = yes

## Define where you want links to end. Modify if you include heatmaps/lines/scatter/histogram)
radius = 0.84r

bezier_radius = 0r
bezier_radius_purity = 0.5
crest         = 0.25
thickness     = 2
color         = blue
</link>
</links>


# Parse information of colors
<<include etc/colors_fonts_patterns.conf>>
<colors>
</colors>

# Housekeeping parameters
<<include etc/housekeeping.conf>>

#!/usr/bin/env python3
#clean_edges_toBipartite_network.py
# -*- coding: utf-8 -*-
##########################
# Given a full network (from Cytoscape) with links between and within two groups
# filter out the within group connections. Save it into .csv to export to Cytoscape.
# This has been coded thinking on 100% output from Genemania.
# You have to export both the Nodes-table and the Edges-table
#########################

# ----------------------------------------------
# Import Libraries
import os
import sys
import pandas as pd
import numpy as np
import argparse
from os.path import join
import pdb

# ----------------------------------------------
# - Parser input options
def options_parse():
    """
    Command Line Options Parser:
    initiate the option parser and return the parsed object
    """
    HELPTEXT = """

    clean_edges_toBipartite_network.py [dev version]


    Author:
    ------
    Victor Montal
    vmontalb [at] santpau [dot] cat

    """

    USAGE = """

    """

    parser = argparse.ArgumentParser(description = HELPTEXT)
    # help text
    h_edgelist  = 'Path to .csv file of edge-lists extracted from Cytoscape'
    h_nodelist  = 'Path to .csv file of node attributes from Cytoscape'
    h_groups    = 'Coma-separeted group names. They MUST be in groupcol'
    h_filtinteract = 'Filter by Interaction type(Co-expression,Co-localization,Genetic Interactions,Pathway,Physical Interactions,Predicted,Shared protein domains)'
    h_outdir    = 'Path where resulting corrected bipartite .csv edge-list will be saved'

    parser.add_argument('--edges',          dest='fedge',       help=h_edgelist,        required = True)
    parser.add_argument('--nodes',          dest='fnode',       help=h_nodelist,        required = True)
    parser.add_argument('--groupfile',      dest='fgroup',      help=h_groups,          required = True)
    parser.add_argument('--filtertype',     dest='filter',      help=h_filtinteract,    required = False,   default="None")
    parser.add_argument('--outdir',         dest='outdir',      help=h_outdir,          required = True)


    args = parser.parse_args()

    # Check options exists
    return args

# - Supplementary functions
def my_print(message):
    """
    print message, then flush stdout
    """
    print(message)
    sys.stdout.flush()


# - Main function
def bipartite_filter(options):
    """
    1) Load edges
    2) Load nodes
    3) Check group nodes included NodeList attributes
    4/1) Save link weights. Useful to compute node degree
    5) Save edge-list filtered to csv
    """
    print("> Loading Node and Edge table..")
    loadnode = pd.read_csv(options.fnode)
    loadedge = pd.read_csv(options.fedge)
    groups = pd.read_csv(options.fgroup)

    print("> Cleaning tables..")
    loadnode.index = loadnode['gene name']      # update pandas rows index
    loadedge.index = loadedge['name']      # update pandas rows index
    groups.index = groups['gene']
    loadedge[['Source','Target','InteractType']] = loadedge['name'].str.split('|', expand=True)

    filtoptions = ["None", "Co-expression", "Co-localization", "Genetic Interactions", "Pathway", "Physical Interactions", "Predicted", "Shared protein domains"]
    if options.filter not in filtoptions:
        print("Invalid link-type filter..")
        print("  Filter valid terms:")
        print("  "+' '.join(filtoptions))
        sys.exit(1)

    if options.filter != "None":
        loadedge = loadedge[loadedge['data type'] == options.filter]

    print("> Checking tables..")
    print(">   Network nodes: "+str(loadnode.shape[0]))
    print(">   Network edges: "+str(loadedge.shape[0]))

    # Compute number of elements per group
    numgenes = dict()
    for cgroup in np.unique(groups['Group']):
        tmp = np.where(groups['Group'] == cgroup)[0]
        numgenes[cgroup] = tmp.size

    print("> Generating Bipartite network (for Gephi)")
    outedge = pd.DataFrame(columns=['Source','Target','Weight','InteractType','GroupType'])
    outnode = pd.DataFrame(columns=['Id','IntraDegree','InterDegree','Group-gene', 'IntraNormDegree','InterNormDegree','APMStotal','APMSmean'])
    idx1 = 1
    idx2 = 1
    for link in loadedge['name'].to_list():
        nodesourc = loadedge.loc[link,'Source']
        nodetarg = loadedge.loc[link,'Target']
        interact = loadedge.loc[link,'InteractType']
        linkweight = loadedge.loc[link,'normalized max weight']


        # Find Source node in NodeList. If not, search in shared_name column from node list
        # This is b/c in the edge-list, some nodes used the name or shared_name
        # This happens for no reason. Genemania cytoscape problem
        spostmpa = np.where(loadnode['name'].str.match(nodesourc) == True )[0]
        spostmpb = np.where(loadnode['shared name'].str.match(nodesourc) == True)[0]
        if spostmpa.size != 0:
            spostmpa = spostmpa[0]
            genesource = loadnode.iloc[spostmpa]['gene name']
            gsource = groups.loc[genesource,'Group']
            nosource = False
        elif spostmpb.size != 0:
            spostmpb = spostmpb[0]
            genesource = loadnode.iloc[spostmpb]['gene name']
            gsource = groups.loc[genesource,'Group']
            nosource = False
        else:
            nosource = True

        # Find Target node in NodeList. If not, search in Shared_name column from node list
        tpostmpa = np.where(loadnode['name'].str.match(nodetarg) == True)[0]
        tpostmpb = np.where(loadnode['shared name'].str.match(nodetarg) == True)[0]
        if tpostmpa.size != 0:
            tpostmpa = tpostmpa[0]
            genetarget = loadnode.iloc[tpostmpa]['gene name']
            gtarget = groups.loc[genetarget,'Group']
            notarget = False
        elif tpostmpb.size != 0:
            tpostmpb = tpostmpb[0]
            genetarget = loadnode.iloc[tpostmpb]['gene name']
            gtarget = groups.loc[genetarget,'Group']
            notarget = False
        else:
            notarget = True

        """
        # If source or target not in nodeList, skip current edge
        if nosource or notarget:
            continue
        """
        # Save intragroup degree
        print(genesource+" "+genetarget)
        if gsource == gtarget:
            #toadd = pd.DataFrame({'Source': [genesource], 'Target': [genetarget], 'Weight': [linkweight], 'InteractType': [interact], 'GroupType' : 'intra'})
            #outedge = outedge.append(toadd, ignore_index = True)

            # Update Node list. If node alredy exists in list, add weight value
            if genesource in outnode['Id'].tolist():
                outnode.loc[genesource,'IntraDegree'] = outnode.loc[genesource,'IntraDegree'] + 1

            else:
                outnode.loc[genesource,'Id'] = genesource
                outnode.loc[genesource,'IntraDegree'] = 1
                outnode.loc[genesource,'InterDegree'] = 0
                outnode.loc[genesource,'APMStotal'] = 0
                outnode.loc[genesource,'Group-gene'] = gsource

            if genetarget in outnode['Id'].tolist():
                outnode.loc[genetarget,'IntraDegree'] = outnode.loc[genetarget,'IntraDegree'] + 1
            else:
                outnode.loc[genetarget,'Id'] = genetarget
                outnode.loc[genetarget,'IntraDegree'] = 1
                outnode.loc[genetarget,'InterDegree'] = 0
                outnode.loc[genetarget,'APMStotal'] = 0
                outnode.loc[genetarget,'Group-gene'] = gtarget

        # Save the intergroup degree
        else:
            toadd = pd.DataFrame({'Source': [genesource], 'Target': [genetarget], 'Weight': [linkweight], 'InteractType': [interact], 'GroupType' : 'inter'})
            outedge = outedge.append(toadd, ignore_index = True)

            # Update Node list. If node alredy exists in list, add weight value
            if genesource in outnode['Id'].tolist():
                outnode.loc[genesource,'InterDegree'] = outnode.loc[genesource,'InterDegree'] + 1
                outnode.loc[genesource,'APMStotal'] = outnode.loc[genesource,'APMStotal'] + groups.loc[genetarget,'APMS']

            else:
                outnode.loc[genesource,'Id'] = genesource
                outnode.loc[genesource,'IntraDegree'] = 0
                outnode.loc[genesource,'InterDegree'] = 1
                outnode.loc[genesource,'APMStotal'] = groups.loc[genetarget,'APMS']
                outnode.loc[genesource,'Group-gene'] = gsource

            if genetarget in outnode['Id'].tolist():
                outnode.loc[genetarget,'InterDegree'] = outnode.loc[genetarget,'InterDegree'] + 1
                outnode.loc[genetarget,'APMStotal'] = outnode.loc[genetarget,'APMStotal'] + groups.loc[genesource,'APMS']
            else:
                outnode.loc[genetarget,'Id'] = genetarget
                outnode.loc[genetarget,'IntraDegree'] = 1
                outnode.loc[genetarget,'InterDegree'] = 1
                outnode.loc[genetarget,'APMStotal'] = groups.loc[genesource,'APMS']
                outnode.loc[genetarget,'Group-gene'] = gtarget

    # Normalize AP-MS by inter-group degree
    print("> Normalizing AP-MS by inter-group degree")
    for cgene in outnode['Id'].tolist():
        if outnode.loc[cgene,'InterDegree'] > 0:
            outnode.loc[cgene,'APMSmean'] = outnode.loc[cgene,'APMStotal'] / outnode.loc[cgene,'InterDegree']
        else:
            outnode.loc[cgene,'APMSmean'] = 0


    # Include non-connected genes
    print("> Including (orphan) genes that are not connected between/within-groups")
    for cgene in groups['gene'].tolist():
        if cgene not in outnode.index.tolist():
            # Include orphan gene to nodelist
            outnode.loc[cgene,'Id'] = cgene
            outnode.loc[cgene,'InterDegree'] = 0
            outnode.loc[cgene,'IntraDegree'] = 0
            outnode.loc[cgene,'APMStotal'] = 0
            outnode.loc[cgene,'APMSmean'] = 0
            outnode.loc[cgene,'Group-gene'] = groups.loc[cgene,'Group']

            """
            # Include orphan gene to edgelist
            toadd = pd.DataFrame({'Source': [cgene], 'Target': ['NOTPRESENT'], 'Weight': [-1], 'InteractType': [interact], 'GroupType' : 'todel'})
            outedge = outedge.append(toadd, ignore_index = True)
            """

    # Compute normalized degree
    print("> Computing normalized Degree (inter- and intra- groups)")
    for i, row in outnode.iterrows():
        cgroup = row['Group-gene']
        # Get number of genes of that group
        if cgroup == 1:
            internorm = numgenes[2]
            intranorm = numgenes[1]
        else:
            internorm = numgenes[1]
            intranorm = numgenes[2]
        # Update normalized degree
        outnode.loc[i,'InterNormDegree'] = (row['InterDegree'] / internorm ) * 100
        outnode.loc[i,'IntraNormDegree'] = (row['IntraDegree'] / intranorm ) * 100

    # Save outputs
    print("> Saving output files (for //Gephi//")
    edgesave = join(options.outdir,'gephi_edgelist.csv')
    outedge.to_csv(edgesave, index=False)
    nodesave = join(options.outdir,'gephi_nodelist.csv')
    outnode.to_csv(nodesave, index=False)

# - Run code
if __name__=="__main__":
    # Command Line options and error checking done here
    options = options_parse()
    bipartite_filter(options)

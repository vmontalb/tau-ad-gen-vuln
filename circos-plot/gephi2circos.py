#!/usr/bin/env python3
#clean_edges_toBipartite_network.py
# -*- coding: utf-8 -*-
##########################
# This script will convert edge-list, node-list files (Gephi format) to input
# input data to CIRCOS circular ploting.
# It will group genes based on GO terms, and prepare the link between genes
# It will also generate the "karyotype" input file, and the "band" file for color
#########################

# ----------------------------------------------
# Import Libraries
import os
import pandas as pd
import numpy as np
import argparse
from os.path import join
import pdb

# ----------------------------------------------
# - Parser input options
def options_parse():
    """
    Command Line Options Parser:
    initiate the option parser and return the parsed object
    """
    HELPTEXT = """

    gephi2circos.py [dev version]


    Author:
    ------
    Victor Montal
    vmontalb [at] santpau [dot] cat

    """

    USAGE = """

    """

    parser = argparse.ArgumentParser(description = HELPTEXT)
    # help text
    h_edgelist  = 'Path to .csv file of edge-lists extracted from Gephi'
    h_nodelist  = 'Path to .csv file of node attributes from Gephi'
    h_go        = 'Path to.csv file with GO term info, as output by Metascape'
    h_fe        = 'Fold enrichment threshold to ged rid off GO-terms (default = 1.5)'
    h_filter    = 'Boolean. If included, genes might be placed in several go terms'
    h_outdir    = 'Path where resulting files for Circos will be output'

    parser.add_argument('--edges',          dest='fedge',       help=h_edgelist,    required = True)
    parser.add_argument('--nodes',          dest='fnode',       help=h_nodelist,    required = True)
    parser.add_argument('--goterms',        dest='fgoterm',     help=h_go,          required = True)
    parser.add_argument('--fe',             dest='fe',          help=h_fe,          required = False, default=1.5)
    parser.add_argument('--skip-filter',    dest = 'filt',      action = 'store_true',  help = h_filter,     required = False, default = False)
    parser.add_argument('--outdir',         dest='outdir',      help=h_outdir,      required = True)


    args = parser.parse_args()

    # Check options exists
    return args

# - Supplementary functions
def my_print(message):
    """
    print message, then flush stdout
    """
    print(message)
    sys.stdout.flush()


# - Main function
def prepareCircos(options):
    """
    1) Load edges
    2) Load nodes
    3) Load GO-term info
    4) Split by GO-terms (i.e create chormosomes for karyotype file) - Create dict
        4.0) Name will be Go number plus Go term
        4.1) Filter GO-terms by Fold-enrichmenet
    5) Loop over all genes in nodelist
        5.1) Search which GO-term in which gene is included, has highest Fold-E
        5.2) Include gene in GO-term
        5.3) If gene not in any GO-Term, place in "others"
    6) Define size of each GO-term. Each gene have to bee value 100
    7) Define "chr"/"go" groups
    8) Define "band"/"genes" for each GO-term
    9) Generate Link files, based on edge-list (Link.dat)
    10) Prepare histogram of dergee for each gene (degree.dat)
    """
    print("> Loading Node, Edge and GO-terms table..")
    loadnode = pd.read_csv(options.fnode)
    loadedge = pd.read_csv(options.fedge)
    loadgo = pd.read_csv(options.fgoterm, delimiter=";")

    print("> Checking tables..")
    print(">   Network nodes: "+str(loadnode.shape[0]))
    print(">   Network edges: "+str(loadedge.shape[0]))

    print("> Cleaning tables..")
    loadnode.index = loadnode['Id']  # update pandas rows index
    loadgo.index = loadgo['Term']    # update pandas rows index
    loadgo = loadgo.sort_values(by='FoldEnrichment', ascending=False) # Sort by FE. Higher the first
    loadgo["Symbols"]= loadgo["Symbols"].str.split(",", n = None, expand = False)  # Convert gene symbols into list

    print("> Filtering GO-terms based on Fold-Enrichment ...")
    tmpgodict = {}
    tmpgodict["Misc"] = list([])
    for cgoterm in loadgo['Term'].to_list():
        cfe = loadgo.loc[cgoterm,'FoldEnrichment']
        if np.float(cfe) > np.float(options.fe):
            tmpgodict[cgoterm] = list([])

    print("> Assigning GO-term to each gene ...")
    for cgene in loadnode['Id'].to_list():
        cgenebool = loadgo['Symbols'].apply(lambda x: cgene in x)
        if options.filt:
            # If gene present in some of the GO-terms
            if any(cgenebool):
                allgo = np.where(cgenebool == True)[0]
                allgo = loadgo.index[allgo]
                for cgo in allgo:
                    # Check if go term is over Fold-Enrichment threshold
                    if cgo in tmpgodict:
                        tmpgodict[cgo].append(cgene)
                    else:
                        tmpgodict["Misc"].append(cgene)
            else:
                tmpgodict["Misc"].append(cgene)
        else:
            # If gene present in some of the GO-terms
            if any(cgenebool):
                firstgo = np.where(cgenebool == True)[0][0]
                firstgo = loadgo.index[firstgo]
                # Check if go term is over Fold-Enrichment threshold
                if firstgo in tmpgodict:
                    tmpgodict[firstgo].append(cgene)
                else:
                    tmpgodict["Misc"].append(cgene)
            else:
                tmpgodict["Misc"].append(cgene)

    # Clean dict (delete GO-terms without genes)
    godict = {}
    for key in tmpgodict:
        clist = tmpgodict[key]
        if len(clist) > 0:
            godict[key] = clist

    # Preparing karyotype (define chromosomes and bands)
    print(". Preparing Karyotype file")
    bandsize = 100
    banddf = pd.DataFrame(columns=['Goterm','id','start','end', 'intradegree', 'interdegree','interweight','intermax'])
    outf = join(options.outdir,'data','karyotype.txt')
    with open(outf,'w') as f:
        chidx = 1
        cgeneidx = 0
        for key in godict:
            if key == 'Misc':
                cname = 'Misc'
            else:
                cname = str(loadgo.loc[key,'Description'] +"_("+loadgo.loc[key,'Term']+")")
            clist = godict[key]
            cchr = "GO"+str(chidx)
            numgenes = len(clist)
            towrite = "chr - "+cchr+" "+cname+" 0 "+str(bandsize*len(clist))+" black \n"
            f.write(towrite)

            # Prepare band inputs
            for idx,cgene in enumerate(clist):
                towrite = "band "+cchr+" "+cgene+" "+cgene+" "+str(idx*bandsize)+" "+str((idx+1)*bandsize)+" black \n"
                f.write(towrite)

                # Append to table. Useful for links afterwards
                banddf.loc[cgeneidx,'Goterm'] = cchr
                banddf.loc[cgeneidx,'id'] = cgene
                banddf.loc[cgeneidx,'start'] = str(idx*bandsize)
                banddf.loc[cgeneidx,'end'] = str((idx+1)*bandsize)
                banddf.loc[cgeneidx,'intradegree'] = str(loadnode.loc[cgene,'IntraDegree'])
                banddf.loc[cgeneidx,'interdegree'] = str(loadnode.loc[cgene,'InterDegree'])

                if False:
                    banddf.loc[cgeneidx,'interweight'] = str(loadnode.loc[cgene,'InterWeighted'])
                    banddf.loc[cgeneidx,'intermax'] = str(loadnode.loc[cgene,'InterMax'])

                if True:
                    banddf.loc[cgeneidx,'apmstotal'] = str(loadnode.loc[cgene,'APMStotal'])
                    banddf.loc[cgeneidx,'apmsmean'] = str(loadnode.loc[cgene,'APMSmean'])

                cgeneidx = cgeneidx + 1

            # Update chromosome position
            chidx = chidx + 1

    # Preparing links (connectogram)
    print(". Preparing links (connectogram)")
    outf = join(options.outdir,'data','links.txt')
    with open(outf,'w') as f:
        for i, row in loadedge.iterrows():
            sourceg = row['Source']
            targetg = row['Target']
            sourcepos = np.where(banddf['id'] == sourceg)[0]
            targetpos = np.where(banddf['id'] == targetg)[0]

            if not options.filt:
                # Parse position in chromosome of each gene
                chrsource = banddf.loc[sourceg,'Goterm']
                chrtarget = banddf.loc[targetg,'Goterm']
                startsource = banddf.loc[sourceg,'start']
                starttarget = banddf.loc[targetg,'start']
                endsource = banddf.loc[sourceg,'end']
                endtarget = banddf.loc[targetg,'end']
                # Write
                towrite = chrsource+" "+startsource+" "+endsource+" "+chrtarget+" "+starttarget+" "+endtarget+" \n"
                f.write(towrite)

            else:
                for cspos in sourcepos:
                    chrsource = banddf.loc[banddf.index[cspos],'Goterm']
                    startsource = banddf.loc[banddf.index[cspos],'start']
                    endsource = banddf.loc[banddf.index[cspos],'end']

                    for ctpos in targetpos:
                        chrtarget = banddf.loc[banddf.index[ctpos],'Goterm']
                        starttarget = banddf.loc[banddf.index[ctpos],'start']
                        endtarget = banddf.loc[banddf.index[ctpos],'end']
                        # Write
                        towrite = chrsource+" "+startsource+" "+endsource+" "+chrtarget+" "+starttarget+" "+endtarget+" \n"
                        f.write(towrite)

    # Preparing BANDs names
    print("Preparing bands labels")
    outf = join(options.outdir,'data','segments.label.txt')
    with open(outf,'w') as f:
        # Apply or not thresholding based on degree
        if False:
            print(".   Keeping labels of genes with top 5% of intra- or inter-degree.")
            for i, row in banddf.iterrows():
                # Compute current go intra and inter threshold
                chrname = row['Goterm']
                tmpp = banddf[banddf["Goterm"] == chrname]
                numgenes = tmpp.shape[0]
                thrposintra = np.int(np.floor(numgenes * 2 / 100))     # Find number of elements and position of X percent
                sortpd = tmpp.sort_values(by='intradegree', ascending=False)   # Sort descending
                intrathresh = sortpd.loc[sortpd.index[thrposintra],'intradegree']  # Get value at thrpos as a threshold

                thrposinter = np.int(np.floor(numgenes * 5 / 100))    # Find number of elements and position of X percent
                sortpd = tmpp.sort_values(by='interdegree', ascending=False)   # Sort descending
                interthresh = sortpd.loc[sortpd.index[thrposinter],'interdegree']  # Get value at thrpos as a threshold

                # Get degree
                cinterd = row['interdegree']
                cintrad = row['intradegree']

                if np.int(cinterd) >= np.int(interthresh) or np.int(cintrad) >= np.int(intrathresh):
                    # Parse parameters
                    start = row['start']
                    end = row['end']
                    label = row['id']

                    # Write
                    towrite = chrname+" "+start+" "+end+" "+label+" \n"
                    f.write(towrite)
        elif False:
            setgenes = ['YWHAZ','HNRNPA1','IGSF8','CD81','HSPA5','CALM1','UBQLN1','EIF4A3','GAPDH',
            'MAPT','RPS3','PIN1','PSMD2','PABPC1','YWHAB','MAPK3','SKP1','ATP1B1','CCDC8',
            'CYC1','LGR4','NR3C1','PRPF8','SDHA','UQCRFS1','HSPB2','NDUFAF3','PFDN1','RCN1','SLC25A5',
            'UCHL5','VAPA','AMOT','APOE','EIF4H','NDUFS4','NECAB2','NOS3','PRKCG','PSME1','PTCH1',
            'SNTA1','ISG15','STXBP5L','SF3B1','DDX1','SPR','PRKCZ','RALY','MRPS26','S100A16',
            'TP53BP2','ADAM22','TUBB2B','IDH3A','CKMT1A','MCFD2','PFKP','CIAPIN1',
            'VBP1','KLC4','MSI2','PCGF1','WRAP73']
            for i, row in banddf.iterrows():
                # Check if gene in set to print
                if row.loc['id'] in setgenes:
                    chrname = row['Goterm']
                    start = row['start']
                    end = row['end']
                    label = row['id']

                    # Write
                    towrite = chrname+" "+start+" "+end+" "+label+" \n"
                    f.write(towrite)

        else:
            for i, row in banddf.iterrows():
                # Parse parameters
                chrname = row['Goterm']
                start = row['start']
                end = row['end']
                label = row['id']

                # Write
                towrite = chrname+" "+start+" "+end+" "+label+" \n"
                f.write(towrite)

    if True:
        # Prepare histogram
        print("Preparing gene-histogram based on intramodule-degree data")
        outf = join(options.outdir,'data','measure.intra-degree.txt')
        with open(outf,'w') as f:
            # Loop over all genes included in the graph/with go term
            for i, row in banddf.iterrows():
                # Parse parameters
                chrname = row['Goterm']
                start = row['start']
                end = row['end']
                cdegree = row['intradegree']

                # Write
                towrite = chrname+" "+start+" "+end+" "+cdegree+" \n"
                f.write(towrite)

    if True:
        # Prepare histogram
        print("Preparing gene-histogram based on intermodule-degree data")
        outf = join(options.outdir,'data','measure.inter-degree.txt')
        with open(outf,'w') as f:
            # Loop over all genes included in the graph/with go term
            for i, row in banddf.iterrows():
                # Parse parameters
                chrname = row['Goterm']
                start = row['start']
                end = row['end']
                cdegree = row['interdegree']

                # Write
                towrite = chrname+" "+start+" "+end+" "+cdegree+" \n"
                f.write(towrite)

    if True:
        # Prepare histogram
        print("Preparing gene-histogram based on AP-MS data")
        outf = join(options.outdir,'data','measure.mean-AP-MS-degree.txt')
        with open(outf,'w') as f:
            # Loop over all genes included in the graph/with go term
            for i, row in banddf.iterrows():
                # Parse parameters
                chrname = row['Goterm']
                start = row['start']
                end = row['end']
                cdegree = row['apmsmean']

                # Write
                towrite = chrname+" "+start+" "+end+" "+cdegree+" \n"
                f.write(towrite)

    if False:
        # Prepare histogram
        print("Preparing gene-histogram based on intermodule-degree data")
        outf = join(options.outdir,'data','measure.interweighted-degree.txt')
        with open(outf,'w') as f:
            # Loop over all genes included in the graph/with go term
            for i, row in banddf.iterrows():
                # Parse parameters
                chrname = row['Goterm']
                start = row['start']
                end = row['end']
                cdegree = row['interweight']

                # Write
                towrite = chrname+" "+start+" "+end+" "+cdegree+" \n"
                f.write(towrite)

    if False:
        # Prepare histogram
        print("Preparing gene-histogram based on intermodule-degree data")
        outf = join(options.outdir,'data','measure.intermax-degree.txt')
        with open(outf,'w') as f:
            # Loop over all genes included in the graph/with go term
            for i, row in banddf.iterrows():
                # Parse parameters
                chrname = row['Goterm']
                start = row['start']
                end = row['end']
                cdegree = row['intermax']

                # Write
                towrite = chrname+" "+start+" "+end+" "+cdegree+" \n"
                f.write(towrite)

    # Prepare heatmap of mean inter-degree by Go-term
    if True:
        print("Preparing GO-term heatmap based on intermodule-degree data")
        outf = join(options.outdir,'data','measure.heatmap.mean-interdegree.goterms.txt')
        with open(outf,'w') as f:
            # Loop over all GO-termns
            for cterm in banddf.Goterm.unique():
                csubset = banddf[banddf['Goterm'] == cterm]
                cmeaninter = np.median(np.int32(csubset['interdegree'].values))

                chrname = cterm
                start = np.min(np.int32(csubset['start'].values))
                end = np.max(np.int32(csubset['end'].values))
                cmeaninter = f"{cmeaninter:.2f}"

                # Write
                towrite = chrname+" "+str(start)+" "+str(end)+" "+cmeaninter+" \n"
                f.write(towrite)

    # Prepare heatmap of mean inter-degree by Go-term
    if True:
        print("Preparing GO-term heatmap based on intramodule-degree data")
        outf = join(options.outdir,'data','measure.heatmap.mean-intradegree.goterms.txt')
        with open(outf,'w') as f:
            # Loop over all GO-termns
            for cterm in banddf.Goterm.unique():
                csubset = banddf[banddf['Goterm'] == cterm]
                cmeaninter = np.median(np.int32(csubset['intradegree'].values))

                chrname = cterm
                start = np.min(np.int32(csubset['start'].values))
                end = np.max(np.int32(csubset['end'].values))
                cmeaninter = f"{cmeaninter:.2f}"

                # Write
                towrite = chrname+" "+str(start)+" "+str(end)+" "+cmeaninter+" \n"
                f.write(towrite)

    # Prepare heatmap of mean inter-degree by Go-term
    if False:
        print("Preparing GO-term heatmap based on weighted inter-module degree data")
        outf = join(options.outdir,'data','measure.heatmap.mean-interweighted.goterms.txt')
        with open(outf,'w') as f:
            # Loop over all GO-termns
            for cterm in banddf.Goterm.unique():
                csubset = banddf[banddf['Goterm'] == cterm]
                cmeaninter = np.median(np.int32(csubset['interweight'].values))

                chrname = cterm
                start = np.min(np.int32(csubset['start'].values))
                end = np.max(np.int32(csubset['end'].values))
                cmeaninter = f"{cmeaninter:.2f}"

                # Write
                towrite = chrname+" "+str(start)+" "+str(end)+" "+cmeaninter+" \n"
                f.write(towrite)

    # Prepare heatmap of mean inter-degree by Go-term
    if False:
        print("Preparing GO-term heatmap based on max intermodule degree data")
        outf = join(options.outdir,'data','measure.heatmap.mean-intramax.goterms.txt')
        with open(outf,'w') as f:
            # Loop over all GO-termns
            for cterm in banddf.Goterm.unique():
                csubset = banddf[banddf['Goterm'] == cterm]
                cmeaninter = np.median(np.int32(csubset['intermax'].values))

                chrname = cterm
                start = np.min(np.int32(csubset['start'].values))
                end = np.max(np.int32(csubset['end'].values))
                cmeaninter = f"{cmeaninter:.2f}"

                # Write
                towrite = chrname+" "+str(start)+" "+str(end)+" "+cmeaninter+" \n"
                f.write(towrite)

    # Prepare heatmap of mean AP-MS by Go-term
    if True:
        print("Preparing GO-term heatmap based on AP-MS expression data")
        outf = join(options.outdir,'data','measure.heatmap.mean-AP-MS.goterms.txt')
        with open(outf,'w') as f:
            # Loop over all GO-termns
            for cterm in banddf.Goterm.unique():
                csubset = banddf[banddf['Goterm'] == cterm]
                cmeaninter = np.mean(np.float32(csubset['apmsmean'].values))

                chrname = cterm
                start = np.min(np.int32(csubset['start'].values))
                end = np.max(np.int32(csubset['end'].values))
                cmeaninter = f"{cmeaninter:.2f}"

                # Write
                towrite = chrname+" "+str(start)+" "+str(end)+" "+cmeaninter+" \n"
                f.write(towrite)

# - Run code
if __name__=="__main__":
    # Command Line options and error checking done here
    options = options_parse()
    prepareCircos(options)




#done

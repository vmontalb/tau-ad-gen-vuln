# tau-PET genetic vulneravility

This repository includes the scripts used to use in our recent publication Montal etal 2022 Sci Trans Medicine

![alt text](img/thumb.png "Montal_etal_2022")

[[_TOC_]]

## Summary
-ToDo-

## Dependencies
* Data: 
  - ADNI: A table file with the imageIDs used in the current project are included in the repo
          Moreover, ADNI data can be requested at: 
  - HABS: https://habs.mgh.harvard.edu/researchers/request-data/

 - Matlab
   - SPM12 
 - Python3.5+
   - Numpy
   - Pandas
   - Scipy
   - Seaborn
   - Matplotlib
   - Nibabel
   - Brainsmash
   - Brainspace

* Packages/tools
  - ABA cortical gene exp: https://figshare.com/articles/dataset/AHBAdata/6852911
  - CIRCOS : http://www.circos.ca/
  - Brain Connectivity toolbox: https://sites.google.com/site/bctnet/
  - Cytoscape: https://cytoscape.org/
 
## Steps to reproduce the manuscript analyses
1. Construct the voxel-wise adjency matrix of tau-PET correlations (Fig2)
   - Construct gray matter mask of abnormal tau-PET values
   - Compute the matrix within the mask
2. Compute the tau-PET backbone network (Fig2)
3. Longitudinal prediction using Powell optimization (Fig2)
4. Identify Tau Network-related genes (TNG) (Fig3)
5. Generate CIRCOS network (Fig5)

6. Robustness of tau-PET backbone (Fig2)
   - Generate auto-correlated surrogate maps of tau-PET using BrainSmash
   - Perform SPIN rotations that acounts for autocorrelation
7. Robustness TNG-pTau genes (Fig5)

## Construct the voxel-wise adjency matrix of tau-PET correlations
The original image is the 4-D concatenated volume of 3-D tau-PET images at the 8mm MNI space. Such image can be obtained using mri_concat or fslmerge.
The first step is to compute the mask where the tau-PET follow a 2-GMM distribution [(Vogel etal 2020)](https://www.nature.com/articles/s41467-020-15701-2).
```bash
python3 gmm_filter_offtarget_tau.py --mni-mask=/path/to/MNI-GM-BIN-MASK.nii.gz --tauvols=/path/to/4D-tau-vols.nii.gz --outdir=/oatg/to/output
```
where `mni-mask` is a binary .nii file with cortical segmentation of the GM (obtained with FAST or Freesurfer), `tauvol` the 4-D tau-PET concatenated .nii image and `outdir` the path where the output images will be stored. The most relevant output is the file `MNI_mask_GM_filtered_offtarget_8mm.nii.gz`.

Once we have the mask, we will compute the adjency matrix running the MatLab script `vol2adjency.m`
Here, we have some input parameters within the script (invol, maskvol, outdir). 
This script will generate one .mat file containgin two vars: netpet - adjency mat of voxel-wise correlation; and mnicoord - MNI space coordinates of each voxel

## Compute the tau-PET backbone network
Next step is to compute the tau-PET backbone. To this end, we will run the `pet_backbone_creation.m` from within MatLab. This script has some input parameters to be modified from within the script. A summary of the steps:
 1. Load tau-PET covariance adjency matrix.
 2. Convert R-value to p-value. Subset to density of top% link. Apply multiple corrections.
 3. Run Node-Aggregation-Algorithm
 4. Post-hoc clean supernodes parcellation
 5. Save outputs

 The two most important outputs are  `adjency_final_network.txt` and `final_network_node_history.txt` which encodes the node2node association of the resulting supernodes, and the voxel number of all voxels that encompass the supernodes, respectively. Moreover, the script returns a `.nii` volume named `MNI_minimal_greaph.nii` which the resulting supernodes map.
 NOTES: This script depends on two matlab functions also included in the repo: `fdr.m` and `r2pv.m`

## Longitudinal prediction using Powell optimization
Next step is to predict the longitudinal tau-PET accumulation within each supernodes. To this end, we curated a list of tau-PET baseline and 1-,2-year followup data. We compute the predicted values running the script:
```bash
python3 powell-opt_individual_prediction_tauPETlong_v2.py --cross-tau=/path/to/4D-baseline.nii --long-tau=/path/to/4D-long.nii --modules=/path/to/MNI_minimal_graph.nii --taunetwork=/path/to/adjency_final_network.txt --outdir=/path/to/save_output
```
This script will compute, individually, the longitudinal prediction. Moreover, it will compute the whole sample spearman Rho between prediced and original tau, the individual prediction accuracy across the supernodes, and the mean squared error of prediction per each of the supernodes

## Identify Tau Network-related genes (TNG)
At this point, we will identify the set of genes that are related to the tau spreading. This is achieved running the Matlab script `gene_differential_expression_network_based_vFornito_vCorr.m`. The steps are:
 1. Load gene expression data from ABA.
 2. Load Glasser parcellation atlas
 3. Load tau backbone info
 4. Compute the gene expression at each supernode (weighted average using the gene from Glasser parcellation)
 5. Compute the pairwise network distance from the starting node (entorhinal) to all the other network supernodes
 6. Compute the correation between network dsitance and gene expression at each supernode
 7. Correct results for multiple comparisons
 8. Generate a null-model randomly shuffle the network-based distance from entorhinal, and re-computing the gene-distance correlation
 9. Store the results

There are some input parameters from the script that have to be modified. The script will output the correlations and the degree of significance based on multiple comparisons and the non-parametric approach. 

NOTES: This script use the curated gene data from [Arnatkeviciute etal 2019]() and the [BNT toolbox](https://sites.google.com/site/bctnet/)


## Generate CIRCOS network
Circos is a fantastic framework to viz circular entworks. Nevertheless, its mainly developed to plot genetic data (mainly Kariotypes). Thus, we develop a series of scripts to parse our gene information to a format that Circos can interpret. The information on how to create the plots can be found in `/circos-plot`. Briefly the steps are:
 1. Generate Protein-Protein network using Cytoscape Genemania toolbox. We input the TNG genes and the pTau interactome genes
 2. Export resulting network to .csv
 3. Filter to only consider bipartite links (i.e TNG vs pTau-interactome)
 4. Compute within and between group degree 
 5. Generate config files for Circos
 6. Plot

Extensive documentation (and examples!) can be found in Circos webpage. 

NOTES: The GO groups used here, are obtained from Metascape GO-analyses.

## Robustness of tau-PET backbone
In order to guarantee that the tau-PET backbone network is robust, we proposed two different approaches, which generate null-models that account for spatial auto-correlation. 

### Tau-PET surrogate maps
The idea behind this analysis, is that the voxel-wise correlation coeficients might be biased due to overall higher tau-PET in some individuals (due to non-specific tracer binding). Thus, we generate surrogate maps of tau-PET, using the [BrainSmash approach](https://www.sciencedirect.com/science/article/pii/S1053811920305243). This approach generate artifical maps that are spatially auto-correlated to the original ones. We generate, for each individual, 500 surrogate maps of tau-PET and re-run the Node Aggregation Algorithm. From the resulting backbone from the surrogate maps, we compared the overlap with the original backbone based on Normalized mutual information. 
The script to generate the surrogate maps is `generate-surrogate-maps.py`. There is also extensive documentation on the Brainsmash python implementation webpage [here](https://brainsmash.readthedocs.io/en/latest/).

### SPIN rotation approach
The idea behind this analyses is to rotate the tau-PET backbone map, to generate surrogate maps with supernodes of the same size, while forcing an auto-correlation with the original backbone. This approach is described in [Alexander-Bloch etal 2018](https://pubmed.ncbi.nlm.nih.gov/29860082/). We tested the similarity between the surrogate rotated-maps and the original backbone using the Normalized mutual information.
The script to generate the spin rotated maps is `generate-spin-rotation-atlas.py`. There is also extensive documentation on the Spin Rotation python implementation webpage [here](https://brainspace.readthedocs.io/en/latest/).

## Robustness TNG-pTau genes
We wanted to guarantee that the high association between our tau network-related genes, and the pTau interactome genes, was specific to this set of genes.
Thus, we generated a null model, where we assess the association between the pTau interactome genes and 577 random genes. 
A python implementation of such appraoch made by our group, with a more extensive description, can be found [here](https://gitlab.com/vmontalb/ctyo-perm) 

## Contact
  Email: victor [dot] montal [at] protonmail [dot] com \
  Twitter: @vmbotwin

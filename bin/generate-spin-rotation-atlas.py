#!/usr/bin/env python
# generate-spin-rotation-atlas.py

#
# Given a a surface atlas, generate N-permutation surrogate maps
#
# Original Author: Victor Montal
# Date_first: Feb-8-2022
# Date:

# ---------------------------
# Import Libraries
from __future__ import print_function
import sys
import argparse
import os
from os.path import join

import numpy as np
import nibabel as nib
import nibabel.freesurfer.mghformat as nibmgh
import nibabel.freesurfer.io as nibfs

from brainspace.null_models.spin import spin_permutations
from brainspace.null_models import SpinPermutations

import pdb
# ---------------------------
# Parser Options
HELPTEXT = """

generate-spin-rotation-atlas.py [dev version]

Given a Freesurfer surface with atlas information (lh and rh), perform Spin
permutations to mantain the spatial-autocorrelation when performing null models


REFERENCES:
-----------
Alexander-Bloch A, Shou H, Liu S, Satterthwaite TD, Glahn DC,
      Shinohara RT, Vandekar SN and Raznahan A (2018). On testing for spatial
      correspondence between maps of human brain structure and function.
      NeuroImage, 178:540-51.

Author:
------
Victor Montal
vmontalb [at] santpau [dot] cat

"""

USAGE = """

"""

def options_parser():
    """
    Command Line Options Parser:
    initiate the option parser and return the parsed object
    """
    parser = argparse.ArgumentParser(description = HELPTEXT,usage = HELPTEXT)

    # help text
    h_lh    = 'LH surface of the atlas'
    h_rh    = 'RH surface of the atlas'
    h_subj  = 'Path to Freesurfer subject where atlas surfaces are created'
    h_nperm = 'Number of Spin Permutation maps to create'
    h_out   = 'Path where output Spin Permutation maps will be stored'


    parser.add_argument('--lh',     dest = 'lhoverlay', action = 'store',   help = h_lh,    required = True)
    parser.add_argument('--rh',     dest = 'rhoverlay', action = 'store',   help = h_rh,    required = True)
    parser.add_argument('--subj',   dest = 'subjfs',    action = 'store',   help = h_subj,  required = True)
    parser.add_argument('--nperm',  dest = 'nperm',     action = 'store',   help = h_nperm, required = True, type = int)
    parser.add_argument('--outpath',dest = 'outpath',   action = 'store',   help = h_out,   required = True)

    args = parser.parse_args()
    return args

# ---------------------------
# Main Code
def spinPermut(options):
    """
    1. Load lh and rh overlay surface
    2. Load lh and rh sphere surface
    3. Generate Nperm Spin Permutations rotations
    """
    print('----------------------')
    print('>> Spin Permutation rotation algorithm <<')
    print('----------------------')
    # Load FS overlay
    print('> Loading freesurfer overlays')
    print('.          File LH: '+options.lhoverlay)
    print('.          File RH: '+options.rhoverlay)
    lh = nibmgh.load(options.lhoverlay).get_fdata()
    lh = np.squeeze(lh)

    rh = nibmgh.load(options.lhoverlay).get_fdata()
    rh = np.squeeze(lh)

    # Load FS sphere
    print('\n> Loading freesurfer surfaces')
    print('.          File LH: '+join(options.subjfs,'surf','lh.sphere'))
    print('.          File RH: '+join(options.subjfs,'surf','rh.sphere'))
    lhsphere = join(options.subjfs,'surf','lh.sphere')
    lhsphere = nibfs.read_geometry(lhsphere)[0]

    rhsphere = join(options.subjfs,'surf','rh.sphere')
    rhsphere = nibfs.read_geometry(rhsphere)[0]

    # Prepare input data
    spheres = { 'lh' : lhsphere,
                'rh' : rhsphere}
    overlay = { 'lh' : lh,
                'rh' : rh}

    # Compute permutations
    print('\n> Generating spin rotation maps')
    print('.          Num permutations: '+str(options.nperm))
    [lhspin, rhspin] = spin_permutations(spheres, overlay, unique=False,
                        n_rep=options.nperm, random_state=None,
                        surface_algorithm='FreeSurfer')

    # Save Spin Permutations
    print('\n> Saving maps')
    for idx in range(0,lhspin.shape[0]):
        citer = "%03d" % (idx)
        coutlh = join(options.outpath,'lh.spinRot-'+citer+'_backboneNodes.sulc')
        coutrh = join(options.outpath,'rh.spinRot-'+citer+'_backboneNodes.sulc')

        nibfs.write_morph_data(coutlh, lhspin[idx,:])
        nibfs.write_morph_data(coutrh, rhspin[idx,:])

if __name__ == "__main__":
    options = options_parser()
    spinPermut(options)

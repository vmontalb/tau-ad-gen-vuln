#!/usr/bin/env python
# powell-opt_individual_prediction_tauPET.py
# -*- coding: utf-8 -*-
##########################
# Script to compute the Beta that optimize the longitudinal prediction of tauPET
#########################



# ----------------------------------------------
# Import Libraries
import sys
import os
from os.path import join
import argparse

import numpy as np
from scipy import stats
from scipy.optimize import minimize
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import nibabel as nib
import pdb

# ----------------------------------------------
# - Parser input options
def options_parse():
    """
    Command Line Options Parser:
    initiate the option parser and return the parsed object
    """
    HELPTEXT = '''

    powell-opt_individual_prediction_tauPET.py [dev version]

    Summary
    -------
    > This script computes the prediction of longitudinal tau-PET in preAD
    > It is based on a simple model:
        TPlong_i = TPcross_i + (SUM(weight_neighbour * TPcross_neigbours) * beta)

    > Model for prediction from Montal etal 2021

    Author:
    ------
    Victor Montal
    vmontalb [at] protonmail [dot] com

    '''

    USAGE = """

    """

    parser = argparse.ArgumentParser(description = HELPTEXT,formatter_class=argparse.RawTextHelpFormatter)
    # help text
    h_crossd    = 'Path to the 4D .nii volume with the crossectional tauPET of input individuals'
    h_longd     = 'Path to the 4D .nii volume with the longitudinal tauPET of input individuals'
    h_module    = 'Path to 3D minimal_graph .nii volume. Each integer represents a super-module'
    h_connmat   = 'Path to tau-PET network. Adjency matrix.'
    h_dists     = 'Ṕath with distance of each super-module to seeds'
    h_outdir    = 'Path where resulting corrected volume will be saved'

    parser.add_argument('--cross-tau',  dest='crossf',      help=h_crossd,  required = True)
    parser.add_argument('--long-tau',   dest='longf',       help=h_longd,   required = True)
    parser.add_argument('--modules',    dest='modulesf',    help=h_module,  required = True)
    parser.add_argument('--taunetwork', dest='netf',        help=h_connmat, required = True)
    parser.add_argument('--outdir' ,    dest='outdir',      help=h_outdir,  required = True)


    args = parser.parse_args()

    if not (args.modulesf.endswith('.nii') or args.modulesf.endswith('.nii.gz')):
        my_print("Invalid format for the modules file. It MUST be nii or nii.gz")
        sys.exit(1)
    if not (args.longf.endswith('.nii') or args.longf.endswith('.nii.gz')):
        my_print("Invalid format for the longitudinal tau-PET file. It MUST be nii or nii.gz")
        sys.exit(1)
    if not (args.crossf.endswith('.nii') or args.crossf.endswith('.nii.gz')):
        my_print("Invalid format for the crossectional tau-PET file. It MUST be nii or nii.gz")
        sys.exit(1)

    # Check options exists
    return args


# ----------------------------------------------
# - Supplementary functions
def my_print(message):
    """
    print message, then flush stdout
    """
    print(message)
    sys.stdout.flush()

def run_cmd(cmd,err_msg):
    """
    execute the comand
    """
    my_print('#@# Command: ' + cmd+'\n', ff)
    retcode = subprocess.Popen(cmd,shell=True, executable='/bin/bash').wait()
    if retcode != 0 :
        my_print('ERROR: '+err_msg, ff)
        sys.exit(1)
    my_print('\n', ff)


def neighboursum(connmat,cmodule,csubjtau):
    """
    This function computes the weighted sum of the neighbours of an specific
    supermodule.
    The idea is to assess the contribution of neighbouring modules (network
    connected modules) to the current module
    This is normalized by each node degree

    INPUT
    -----
      - connmat  : tau-PET network. NODES x NODES size.
      - cmodule  : Integer. Specify the current module. This refers to the central
                  node were we will compute its neighbours contribution
      - csubjtau : Crossectional tau-PET for current individual. (1 x N-modules) size
    """

    # Get links from current module
    cconn = connmat[cmodule,:]
    # Get degree
    dd = np.sum(cconn > 0)
    # Weighted sum
    csum = np.dot(cconn,csubjtau)
    csum = csum /dd
    return csum



def costpetpredictio(beta,longreald,crossreald,neighbourd):
    """
    This function computes the squared difference between the real tau-PET long
    and the predicted. The prediction is based on:
        TPlong_i = REWRITE

    The prediction depends on a Beta value, wich will be changed depending on
    a Powell optimization function iterative process.

    The cost function is the mean squared difference between predicted longitudinal
    value and

    NOTE: This will be done per each super-module independently

    INPUT
    -----
      - beta        : Weight to the neighbouring weighted additive.
      - longreald   : Vector of real longitudinal tau-PET. Data for one super-module
      - crossreald  : Vector of real crossectional tau-PET. Data for one super-module
      - neighbourd  : Vector of neighbouring wieghted sum of tau-PET. Data for one super-module
    """
    # Compute the prediction
    predict = crossreald + neighbourd * (np.exp(crossreald*beta)-crossreald)
    # Compute the cost (mean squared difference)
    cost = np.sum(np.square(longreald - predict))
    return cost

def MNIprojection(imgref,indata,outpath):
    """
    This function will project the indata (each individual)
    to MNI space (same header info as imgref .nii volume)

    INPUT
    -----
      - imgref  : Nibabel.nifti object. Info of header to save
      - indata  : Vector of 1xN-modules to project to imgref vol
      - outpath : Path where image will be saved
    """
    # Get reference image
    imdata = imgref.get_fdata()
    rsimdata = imdata.reshape(imdata.shape[0]*imdata.shape[1]*imdata.shape[2],-1)
    # Loop over all modules and parse indata
    rsout = np.zeros_like(rsimdata)
    allmodules = np.unique(rsimdata)[1:]  # skip zero
    for idx,cmod in enumerate(allmodules):
        pos = np.where(rsimdata == cmod)[0]
        rsout[pos] = indata[idx]
    # Save vol
    outvol = rsout.reshape(imdata.shape[0],imdata.shape[1],imdata.shape[2])
    outnib = nib.Nifti1Image(outvol,imgref.affine,imgref.header)
    nib.save(outnib,outpath)


# ----------------------------------------------
# - Main function
def longprediction(options):
    """
    1 - Load input files
    2 - Compute mean tauPET for each module (cross and long)
    3 - Compute sum _ neighbouring nodes (cross) fo each module
    4 - Solve TPlong_i = TPcross_i + (Neighbouring_i)* beta (where i = module)
    5 - Compute estimated long using betas (can be loaded from file or previously computed in script)
    6 - Compute Spearman correlation between estimated_long and real_long (for each individual)
    7 - Compute individual Goodness of fit (sum(real-predicted)^2)
    8 - Compute module Goodness of fit
    """

    my_print("> Loading input files")
    incross = nib.load(options.crossf)
    crossd = incross.get_fdata()
    rscrossd = crossd.reshape(crossd.shape[0]*crossd.shape[1]*crossd.shape[2], -1)

    inlong = nib.load(options.longf)
    longd = inlong.get_fdata()
    rslongd = longd.reshape(longd.shape[0]*longd.shape[1]*longd.shape[2], -1)

    inmodules = nib.load(options.modulesf)
    modulesd = inmodules.get_fdata()
    rsmodulesd = modulesd.reshape(modulesd.shape[0]*modulesd.shape[1]*modulesd.shape[2], -1)

    inconnmat = np.loadtxt(options.netf)

    my_print('>     Checking dimensions cross and long match')
    if crossd.shape != longd.shape:
        my_print("Woha! Cross tau-PET image dimensions does not match with Long tau-PET image dimensions. Double-check")
        sys.exit(1)


    my_print("========")
    my_print(" INPUT PARAMETERS: ")
    my_print("========")
    my_print(".   Number individuals: "+str(rscrossd.shape[1]))
    my_print(".   tau-PET image dimensions: "+str(crossd.shape[0:3]))
    my_print(".   Number of super-modules: "+str(inconnmat.shape[0]))
    my_print("")


    my_print("========")
    my_print(" STARTING ALGORITHM    (O.o)/ ")
    my_print("========")

# -- Preparing data for asses predicction
    my_print("> Computing mean tau-PET SUVr for each super-module")
    my_print(">     This is done both for cross and long tau-PET")
    modules = np.unique(rsmodulesd)[1:]  # skip zero
    meancrossd = np.zeros([modules.size, rscrossd.shape[1]])
    meanlongd = np.zeros([modules.size, rslongd.shape[1]])

    # Loop over all modules
    for idx,cmod in enumerate(modules):
        ## Find positions of cmod
        cpos = np.where(rsmodulesd == cmod)[0]
        ## Compute mean
        cmeancross = np.mean(rscrossd[cpos,:], axis=0)
        cmeanlong = np.mean(rslongd[cpos,:], axis=0)
        ## Parse mean
        meancrossd[idx,:] = cmeancross
        meanlongd[idx,:] = cmeanlong


    my_print("> Computing weighted sum of neighbour supermodules")
    neighbourd = np.zeros_like(meancrossd)
    # Loop over all individuals
    for cindv in range(0,meancrossd.shape[1]):
        # Get current (crossectional) individual tau-PET module mean
        ctaupet = meancrossd[:,cindv]
        # Loop over all nodes
        for idx,cmod in enumerate(modules):
            # Get neighbour weighted sum
            neighbourd[idx,cindv] = neighboursum(inconnmat,idx,ctaupet)

    my_print("> Computing regularization Beta parameter for optimal longitudinal tau-PET prediction")
    # Loop over all super-modules
    regulbeta = np.zeros([modules.size])
    for idx,cmod in enumerate(modules):
        #my_print(">      Working with supermodule: "+str(cmod))
        # Get realLong realCross and neighbour for all individuals for current module
        clongreald = meanlongd[idx,:]
        ccrossreald = meancrossd[idx,:]
        cneighbourd = neighbourd[idx,:]

        # Powell minimization to get optimal beta regularization value
        x0 = .1
        opt = minimize(costpetpredictio,x0, args=(clongreald,ccrossreald,cneighbourd), method="Powell", bounds=[(0,1)])

        # Parse beta
        regulbeta[idx] = opt.x

    my_print("> Calculating tau-PET prediction based on regularization Beta parameters")
    predictd = np.zeros_like(neighbourd)
    # Loop over all modules
    for idx,cmod in enumerate(modules):
        cregul = regulbeta[idx]
        ccrossreald = meancrossd[idx,:]
        cneighbourd = neighbourd[idx,:]

        # Compute prediction
        #predictd[idx,:] = ccrossreald + (cneighbourd*cregul)
        predictd[idx,:] = ccrossreald + cneighbourd * (np.exp(ccrossreald*cregul)-ccrossreald)

    my_print("> Saving scatterplot and Nifti volume of the prediction (by real_long)")
    for csubj in range(0,predictd.shape[1]):
        # outputs
        coutscatter = join(options.outdir,'scatters_individuals','subj_'+str(csubj)+'_scatter.svg')
        outniffti = join(options.outdir,'indv_prediction','subj_'+str(csubj)+'_predicted.nii.gz')

        # Scatter
        clong = meanlongd[:,csubj]
        ccross = meancrossd[:,csubj]
        cpredict = predictd[:,csubj]
        tmpdf = pd.DataFrame({'Real Long':clong, 'Predict Long': cpredict, 'Cross': ccross})
        fig, ax = plt.subplots()
        ## Regression predicted long vs real long
        slope, intercept, r_value, pv, se = stats.linregress(tmpdf['Predict Long'],tmpdf['Real Long'])
        sns.regplot(x="Predict Long", y="Real Long", data=tmpdf,
            ci=None, ax=ax, label="Long Prediction vs Real Long slope: y={0:.3f}x+{1:.2f}".format(slope, intercept)).legend(loc="best")
        fig.savefig(coutscatter, format="svg")
        plt.close()

        # Individual predicted volume
        MNIprojection(inmodules,cpredict,outniffti)


    my_print("> Computing individual error (real long vs predicted long and real long vs cross)")
    errorpredictd = np.zeros_like(predictd)
    errorcrossd = np.zeros_like(predictd)
    for csubj in range(0,predictd.shape[1]):
        # Outputs
        coutcrossvol = join(options.outdir,'indv_error_crossVSlong','vol','subj_'+str(csubj)+'_crossError.nii.gz')
        coutcrossscatter = join(options.outdir,'indv_error_crossVSlong','scatter','subj_'+str(csubj)+'_crossError.svg')
        coutpredictvol = join(options.outdir,'indv_error_predictVSlong','vol','subj_'+str(csubj)+'_predictError.nii.gz')
        coutpredictscatter = join(options.outdir,'indv_error_predictVSlong','scatter','subj_'+str(csubj)+'_predictError.svg')

        # Save prediction error
        errorpredictd[:,csubj] = np.abs(meanlongd[:,csubj] - predictd[:,csubj])
        errorcrossd[:,csubj] = np.abs(meanlongd[:,csubj] - predictd[:,csubj])
        MNIprojection(inmodules,errorpredictd[:,csubj],coutpredictvol)
        MNIprojection(inmodules,errorcrossd[:,csubj],coutcrossvol)

        # Save scatter
        tmpaxes = np.arange(0,errorpredictd.shape[0],1)
        tmpdf = pd.DataFrame({'predict error':errorpredictd[:,csubj], 'Cross error': errorcrossd[:,csubj], 'Nun subject': tmpaxes})
        fig, ax = plt.subplots()
        sns.scatterplot(data=tmpdf, x='Nun subject',y='predict error', ax=ax)
        fig.savefig(coutpredictscatter, format="svg")
        plt.close()
        fig, ax = plt.subplots()
        sns.scatterplot(data=tmpdf, x='Nun subject',y='Cross error', ax=ax)
        fig.savefig(coutcrossscatter, format="svg")
        plt.close()



    my_print("> Computing Spearman correlation (per individual)")
    indvspearman = np.zeros([predictd.shape[1]])
    indvspearmanp = np.zeros([predictd.shape[1]])
    for csubj in range(0,predictd.shape[1]):
        clongreald = meanlongd[:,csubj]
        clongpredictd = predictd[:,csubj]
        rho,pval = stats.spearmanr(clongreald,clongpredictd)

        indvspearman[csubj] = rho
        indvspearmanp[csubj] = pval

    # Save plot
    outfig = join(options.outdir,'final_graphs','spearman_corr_pval_allIndividuals.svg')
    tmpaxes = np.arange(0,errorpredictd.shape[1],1)
    tmpdf = pd.DataFrame({'Spearman':indvspearman, 'Pvalue':indvspearmanp, 'Num-Subject':tmpaxes})
    fig, ax = plt.subplots()
    sns.lineplot(data=tmpdf, x='Num-Subject',y='Spearman', ax=ax, marker='o', linestyle='--')
    #sns.scatterplot(data=tmpdf, x='Num-Subject',y='Pvalue', ax=ax)
    fig.savefig(outfig, format="svg")
    plt.close()


    my_print("> Saving Spearman correlation (all individuals)")
    outfig = join(options.outdir,'final_graphs','spearman_corr_wholeSample.svg')
    dtmpr = meanlongd.reshape(meanlongd.shape[0]*meanlongd.shape[1])
    dtmpp = predictd.reshape(predictd.shape[0]*predictd.shape[1])
    tmp = pd.DataFrame({'real':dtmpr, 'predict':dtmpp})
    fig, ax = plt.subplots()
    rho, pv = stats.spearmanr(dtmpr,dtmpp)
    sns.regplot(x="real", y="predict", data=tmp, ax=ax,
      ci=None, label="Rho={0:.3f}".format(rho)).legend(loc="best")
    fig.savefig(outfig, format="svg")

    my_print("> Saving mean squared error of prediction (all individuals)")
    indverror = np.zeros([predictd.shape[1]])
    for csubj in range(0,predictd.shape[1]):
        clongreald = meanlongd[:,csubj]
        clongpredictd = predictd[:,csubj]
        cerror = np.mean(np.square(clongreald - clongpredictd))

        indverror[csubj] = cerror

    # Save plot
    outfig = join(options.outdir,'final_graphs','sum_squared_error_individuals.svg')
    tmpaxes = np.arange(0,errorpredictd.shape[1],1)
    tmpdf = pd.DataFrame({'Error':indverror, 'Num-Subject':tmpaxes})
    fig, ax = plt.subplots()
    sns.lineplot(data=tmpdf, x='Num-Subject',y='Error', ax=ax)
    fig.savefig(outfig, format="svg")
    plt.close()

    my_print("> Saving mean squared error of prediction (all ROIs)")
    roierror = np.zeros([predictd.shape[0]])
    for croi in range(0,predictd.shape[0]):
        clongreald = meanlongd[croi,:]
        clongpredictd = predictd[croi,:]
        cerror = np.mean(np.square(clongreald - clongpredictd))

        roierror[croi] = cerror

    # Project to MNI
    outnifti = join(options.outdir,'roi_standardError','MSE_ROI_error.nii.gz')
    MNIprojection(inmodules,roierror, outnifti)

    # Save plot
    outfig = join(options.outdir,'final_graphs','sum_squared_error_ROIs.svg')
    tmpaxes = np.arange(0,errorpredictd.shape[0],1)
    tmpdf = pd.DataFrame({'Error':roierror, 'Num-Subject':tmpaxes})
    fig, ax = plt.subplots()
    sns.lineplot(data=tmpdf, x='Num-Subject',y='Error', ax=ax)
    fig.savefig(outfig, format="svg")
    plt.close()

    my_print("> Saving .txt files")
    outpath = join(options.outdir,'output_matrixs', )


    # Repeat the prediction with a permuted matrix of connectivity
    ## - This is basically repeat the previous steps shuffling the conn matrix
    nperm = 100
    my_print("> Generating a null model of the predictions using "+str(nperm)+" permutations")
    # Define variables for all permutations
    nullpredict = np.zeros([meancrossd.shape[0], meancrossd.shape[1], nperm])  # N-modules by N-subject by 1000 permutations
    nullmean = np.zeros_like(meancrossd)
    nullstd = np.zeros_like(meancrossd)
    permrho = np.zeros([nperm])
    rng = np.random.default_rng()  # Numpy permutation generator
    # Loop over all permutations
    for cperm in range(0,nperm):
        if cperm % 100 == 0:
            my_print("> .. iteration:"+str(cperm))
        # Permut connectivity matrix
        permutconn = rng.permuted(inconnmat)
        permutconn = permutconn * (np.abs(np.identity(permutconn.shape[1]) -1))

        # Re-compute neighbour sum
        pneighbourd = np.zeros_like(meancrossd)
        # Loop over all individuals
        for cindv in range(0,meancrossd.shape[1]):
            # Get current (crossectional) individual tau-PET module mean
            ctaupet = meancrossd[:,cindv]
            # Loop over all nodes
            for idx,cmod in enumerate(modules):
                # Get neighbour weighted sum
                pneighbourd[idx,cindv] = neighboursum(permutconn,idx,ctaupet)

        # Re-compute regularized beta value using powell
        pregulbeta = np.zeros([modules.size])
        for idx,cmod in enumerate(modules):
            #my_print(">      Working with supermodule: "+str(cmod))
            # Get realLong realCross and neighbour for all individuals for current module
            clongreald = meanlongd[idx,:]
            ccrossreald = meancrossd[idx,:]
            cneighbourd = pneighbourd[idx,:]

            # Powell minimization to get optimal beta regularization value
            x0 = .1
            opt = minimize(costpetpredictio,x0, args=(clongreald,ccrossreald,cneighbourd), method="Powell", bounds=[(0,1)])

            # Parse beta
            pregulbeta[idx] = opt.x

        # Re-compute the prediction based on the regularized
        ppredictd = np.zeros_like(pneighbourd)
        # Loop over all modules
        for idx,cmod in enumerate(modules):
            cregul = pregulbeta[idx]
            ccrossreald = meancrossd[idx,:]
            cneighbourd = pneighbourd[idx,:]

            # Compute prediction
            ppredictd[idx,:] = ccrossreald + cneighbourd * (np.exp(ccrossreald*cregul)-ccrossreald)

        # Save all the prediction values
        nullpredict[:,:,cperm] = ppredictd

        # Re-compute spearman
        rsppredicted = ppredictd.reshape(ppredictd.shape[0]*ppredictd.shape[1])
        rsreal = meanlongd.reshape(meanlongd.shape[0]*meanlongd.shape[1])
        rho, pv = stats.spearmanr(rsreal,rsppredicted)
        permrho[cperm] = rho

    # Seaborn all points for all permutations
    rspredictedperm = nullpredict.reshape(nullpredict.shape[0]* nullpredict.shape[1]* nullpredict.shape[2])
    rsrealrep = np.tile(rsreal, nperm)


    outfig = join(options.outdir,'final_graphs','spearman_corr_wholeSample_nullModel_permutations.png')
    tmp = pd.DataFrame({'real':rsrealrep, 'predict':rspredictedperm})
    fig, ax = plt.subplots()
    sns.lineplot(data = tmp, x="real", y="predict")
    fig.savefig(outfig, format="png")

    # Plot histogram of the spearman for each of the random
    outfig = join(options.outdir,'final_graphs','histogram_spearman_permut_nullmodel.png')
    tmp = pd.DataFrame({'rhos':permrho})
    fig, ax = plt.subplots()
    sns.histplot(data = tmp, x="rhos")
    fig.savefig(outfig, format="png")

    # Plot ROI error for the null model (boxplot)
    proierror = np.zeros([ppredictd.shape[0], nperm])
    for croi in range(roierror.shape[0]):
        clongreald = meanlongd[croi,:]
        clongpredictd = nullpredict[croi,:,:]
        sqrt = np.square(clongreald[:, np.newaxis] - clongpredictd)
        cerror = np.mean(sqrt, axis = 0)

        proierror[croi,:] = cerror

    roiorder = np.arange(0,proierror.shape[0],1)
    rsroiorder = np.tile(roiorder, nperm)
    rsroierror = proierror.reshape(proierror.shape[0]*proierror.shape[1])
    pdb.set_trace()
    tmpaxes = np.arange(0,proierror.shape[0],1)
    outfig = join(options.outdir,'final_graphs','boxplot_roierror_permut_nullmodel.png')
    tmp = pd.DataFrame({'roi-error':rsroierror, 'ROI':rsroiorder})
    tmp2 = pd.DataFrame({'orig-error':roierror, 'ROI':tmpaxes})
    fig, ax = plt.subplots()
    sns.stripplot(data=tmp2, x='ROI', y='orig-error', ax=ax)
    sns.boxplot(data = tmp, x="ROI", y='roi-error', ax=ax)
    fig.savefig(outfig, format="png")

# ----------------------------------------------
# - Run code
if __name__=="__main__":
    # Command Line options and error checking done here
    options = options_parse()
    longprediction(options)

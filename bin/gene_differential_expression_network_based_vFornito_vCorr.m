clear;
clc;



%% Info Script
% ============
% This script will compute unsing the global network, the pattern of
% differential expressed genes. The idea is to see which genes are constant
% and which ones change more when accounting for network proprieties. To
% achieve this, we will compute the gene difference between the seed region
% and all the other regions, using the distance between seed regions and
% other region to ponderate the differences.
% Just those genes that are change/nonchange their expressed (>2 std) will be
% considered as significant.
% 
% In addition, for each Module, the script will return and save which genes
% are over/under expressed (zscore <> 2).
%
% Finally, it will save which Desikan ROIs have been used for each
% module gene-expression values. In addition, it will save the genes
%
% NOTES: The input network must be an adjency matrix, where each node
% follows the order of the modules (first row = module1, etc)
%
% NOTES2: This script used Brain Connectivity Toolbox
addpath('/home/slieped/Documents/Work/Software/BCT/2019_03_03_BCT');
addpath('/media/slieped/TheShire/WORK/Boston2019/AD-network/APR27/');


%% Load data
% ==========
% Define inputs
% - insurfgraph: Freesurfer surface. Each vertex value represent the module
% obtained from the minimal-graph algorithm.
% - network: Txt file with the network (edge-list file).
% - ingenesmat: Txt matrix file with gene by ROI expression
% - inaparc: Freesurfer .annot file of the ROIs used for gene expression
% - inroilist: Txt file with list of ordered ROIS used to compute the
% ingenesmat matrix.
% - ingenelist: Txt file with list of ordered GENES used to compute the
% ingenesmat matrix.
% - outpath: Path were results will be saved.
%

fprintf("> Loading input files \n")
lhinsurfgraph = '/media/slieped/TheShire/WORK/Boston2019/AD-network/ADNI+HABS/TAU_PET/node-aggregation/q_0.005_fwe_rs8/top_20_dense_conn/final_network/lh.MNI_minimal_graph.mgh';
rhinsurfgraph = '/media/slieped/TheShire/WORK/Boston2019/AD-network/ADNI+HABS/TAU_PET/node-aggregation/q_0.005_fwe_rs8/top_20_dense_conn/final_network/rh.MNI_minimal_graph.mgh';
ingenesmat = '/media/slieped/TheShire/WORK/HSP/Tools/AHBA_Fornito/data_expression/ROIxGene_HCP_RNAseq.mat';
%ingenesmat = '/media/slieped/TheShire/WORK/HSP/Neuroimaging/Tools/AHBA_Fornito/data_expression/ROIxGene_HCP_INT.mat';
lhinparcel = '/pubsw/freesurfer/freesurfer7.1.0/subjects/fsaverage/label/lh.HCP-MMP1.annot';
rhinparcel = '/pubsw/freesurfer/freesurfer7.1.0/subjects/fsaverage/label/rh.HCP-MMP1.annot';
innetwork = '/media/slieped/TheShire/WORK/Boston2019/AD-network/ADNI+HABS/TAU_PET/node-aggregation/q_0.005_fwe_rs8/top_20_dense_conn/final_network/adjency_final_network.txt';
inneurogenes = '/media/slieped/TheShire/WORK/Boston2019/AD-network/ADNI+HABS/TAU_PET/utils/neuro-genes.list';

seedmodule = [48];  % lh entorhinal
%seedmodule = [29]; % rh entorhinal

multicomp = 'fdr';


outpath = '/media/slieped/TheShire/WORK/Boston2019/AD-network/ADNI+HABS/TAU_PET/gene_analysis/test';
% Prepare output folder
if isdir([outpath]) == 0
    mkdir([outpath]);
end

% Load input surface files
[lhsurfgraph, lhmri] = fs_read_Y(lhinsurfgraph);
[rhsurfgraph, rhmri] = fs_read_Y(rhinsurfgraph);
[~,lhparcel, lhparcelrois] = read_annotation(lhinparcel);
[~,rhparcel, rhparcelrois] = read_annotation(rhinparcel);

% Load gene information
geneFornito = load(ingenesmat);
genesmat = [geneFornito.parcelExpression(:,2:end)' geneFornito.parcelExpression(:,2:end)'];  % First column refers to ROI id
geneid = geneFornito.probeInformation.GeneSymbol;

% Load neuro-genes list and filter
fid = fopen(inneurogenes, 'r');
neurogenes = textscan(fid,'%s');
fclose(fid);
neurogenes = neurogenes{1};

%[geneid,idx1,~] = intersect(geneid,neurogenes);
%genesmat = genesmat(idx1,1:end);

% Load network
datanet = load(innetwork);

% Compute min distance from all seeds
invnet = 1./datanet;
invnet(datanet==0) = 0;
distcurr = distance_wei(invnet);

% Concat two hemispheres into one
fprintf("> Merging two-hemisphere-space data into one space \n")
surfgraph = [lhsurfgraph rhsurfgraph];
lhparcel = lhparcel * 99;
lhparcelrois.table(:,5) = lhparcelrois.table(:,5)*99;
lhparcelnames = lhparcelrois.struct_names;
rhparcelnames = rhparcelrois.struct_names;

parcelnames = [lhparcelnames(2:end); rhparcelnames(2:end)];
parcelid = [lhparcelrois.table(2:end,5); rhparcelrois.table(2:end,5)];
parcelLabels = [lhparcel; rhparcel];



% Compute Module specific over/under expression genes profile
% ===========================================================
fprintf("> Computing gene expression per module (linear combination HCP atlas) \n");
fprintf(">      ...  and computing over/under expressed genes for each module \n");
networkmodule = unique(surfgraph);
genesmodulesover = cell(1,length(datanet));
genesmodulesunder = cell(1,length(datanet));
roisusedmodule = cell(length(datanet),1);
modulesgeneprofile = zeros(length(geneid), length(datanet));   % gene by module network
for idx1=1:length(datanet)
    % Iterate over all modules
    cmodule = idx1;
    saverois = surfgraph * 0;

    % Check that current module have cortical values (i.e in surface)
    if ismember(idx1,networkmodule)
        fprintf(">      Working with module: %s \n", num2str(idx1));
        
        % Initialize outputs 
        outpathcmodule = [outpath '/module_' num2str(idx1) '_gene_results'];
        if isdir([outpathcmodule]) == 0
            mkdir([outpathcmodule]);
        end
        
        % Save current module into .mgh surface file        
        posmodule = find(surfgraph == cmodule);
        currmodsurf = surfgraph * 0;
        currmodsurf(posmodule) = 1;
        lhcurrmod = currmodsurf(1:length(lhsurfgraph));
        rhcurrmod = currmodsurf(length(lhsurfgraph)+1:end);
        fs_write_Y(lhcurrmod,lhmri,[outpathcmodule '/lh.current_module_projection.mgh']);
        fs_write_Y(rhcurrmod,rhmri,[outpathcmodule '/rh.current_module_projection.mgh']);
        
        % Find overlaping rois
        roioverlap = unique(parcelLabels(posmodule));
        percentoverlap = zeros(length(roioverlap),1);
        roisoverlap = cell(2,length(roioverlap));
        
        % Save overlaping parcellation ROIs into .mgh file
        for idx2=1:length(roioverlap)
            % Check that ROI is not ventricle/corpuscallosum regions
            if ~isempty(find(roioverlap(idx2) ==  parcelid))
                posroi = find(parcelLabels == roioverlap(idx2));
                saverois(posroi) = idx2;
            else
                continue;
            end
        end
        lhrois = saverois(1:length(lhsurfgraph));
        rhrois = saverois(length(lhsurfgraph)+1:end);
        fs_write_Y(lhrois,lhmri,[outpathcmodule '/lh.rois_used_local_genexpression.mgh']);
        fs_write_Y(rhrois,rhmri,[outpathcmodule '/rh.rois_used_local_genexpression.mgh']);

        % Compute percent overlap between module and parcellation ROIs
        % Exclude those non-brain regions
        froioverlap = [];
        fpercentoverlap = [];
        for idx2=1:length(roioverlap)
            posroi = find(parcelLabels == roioverlap(idx2));
            overlap = intersect(posmodule,posroi);
            percentoverlap(idx2) = (length(overlap) / length(posmodule));
            
            % Check that ROI is not ventricle/corpuscallosum regions
            tmp = find(roioverlap(idx2) ==  parcelid);
            if ~isempty(tmp)
                froioverlap = [froioverlap roioverlap(idx2)];
                fpercentoverlap = [fpercentoverlap percentoverlap(idx2)];
                croiname = parcelnames{tmp};
                roisoverlap{2,idx2} = percentoverlap(idx2);
                roisoverlap{1,idx2} = croiname;
            else
                croiname = 'Non-brain_region_(ventricles_CC)';
                roisoverlap{2,idx2} = percentoverlap(idx2);
                roisoverlap{1,idx2} = croiname;                
            end
        end
        
        % Save rois name and percent used to construct module gene
        % expression
        outpathpercent = [outpath '/module_' num2str(idx1) '_gene_results/percent_rois_used.txt'];
        fid = fopen(outpathpercent,'w');
        fprintf(fid,'%s \n', roisoverlap{:});
        fclose(fid);

        % Find position of each overlaping ROI into the gene expresion matrix
        overlaproipos = zeros(length(froioverlap),1);
        for idx2=1:length(froioverlap)
            tmp = find(froioverlap(idx2) ==  parcelid);
            overlaproipos(idx2) = tmp;
        end

        % Compute each gene mean expression weighted by percentage of ROI    
        overlapingroisgene = genesmat(:,overlaproipos);  % Check to change to normgenesmat
        overlapingroisgene(isnan(overlapingroisgene)) = 0;  % Skip 3 ROIS withou any sample inside.
        modulegeneprofile = overlapingroisgene * fpercentoverlap';
        modulesgeneprofile(:,idx1) = modulegeneprofile;
        
        % Compute the over expressed and under expressed genes for each
        % module
        poscmoduleover = find(modulegeneprofile > (mean(modulegeneprofile)+2*std(modulegeneprofile)));
        poscmoduleunder = find(modulegeneprofile < (mean(modulegeneprofile)-2*std(modulegeneprofile)));
        genescmoduleover = geneid(poscmoduleover);
        genescmoduleunder = geneid(poscmoduleunder);
        genesmodulesover{idx1} = genescmoduleover;
        genesmodulesunder{idx1} = genescmoduleunder;
                
        % Save current module over/under expressed
%         fid = fopen([outpathcmodule '/significant_over-expressed_genes.txt'],'w');
%         fprintf(fid,'%s \n', genescmoduleover{:});
%         fclose(fid);
%         fid = fopen([outpathcmodule '/significant_under-expressed_genes.txt'],'w');
%         fprintf(fid,'%s \n', genescmoduleunder{:});
%         fclose(fid);
        
    else
        modulesgeneprofile(:,idx1) = 0;
        genesmodulesover{idx1} = {' '}; 
        genesmodulesunder{idx1} = {' '};
    end
end
% Save all modules gene profile
% outpathmodulegene = [outpath '/modules_gene_profile.txt'];
% dlmwrite(outpathmodulegene,modulesgeneprofile);


% Compute the network-based gene differential expression
% ======================================================
fprintf("> Create the network-based gene differential expression profile for each module \n");
ntwkbasedcorr = zeros(1,size(modulesgeneprofile,1));
ntwkbasedpval = zeros(1,size(modulesgeneprofile,1));

seeddist = distcurr(seedmodule,:);
seeddist = [seeddist(1:seedmodule-1) seeddist(seedmodule+1:end)]; %exclude seed 
for idx1=1:size(modulesgeneprofile,1)  % Loop over all genes
    % Compute network-specific correlations using seed distance
    gexp = modulesgeneprofile(idx1,:);
    cgexp = [gexp(1,1:seedmodule-1) gexp(1,seedmodule+1:end)];
    [cc,pp] = corr(cgexp(:),seeddist(:), 'Type', 'Spearman');
    ntwkbasedcorr(idx1) = cc;
    ntwkbasedpval(idx1) = pp;
    
end
% Compute gene specific variance
genevar = std(modulesgeneprofile')';

% Multiple comparisons
if multicomp == 'fdr'
    [n_signif,index_signif] = fdr(ntwkbasedpval, 0.001,'original','mean');    
else
    % Compute Bonferroni correction of pvalue
    thr = 0.05;
    corr_thr = thr / size(genesmat,1);
    index_signif = find(ntwkbasedpval < corr_thr);    
end

% Save non-permutation sign genes
idxuncorr = find(ntwkbasedpval <= 0.00001);
uncorrgenes = cell(5,length(idxuncorr));
uncorrgenes(1,:) = geneid(idxuncorr);
uncorrgenes(2,:) = num2cell(ntwkbasedcorr(idxuncorr));
uncorrgenes(3,:) = num2cell(ntwkbasedpval(idxuncorr));
uncorrgenes(4,:) = num2cell(0);
tmp = ismember(idxuncorr,index_signif);
uncorrgenes(4,tmp) = num2cell(1);
uncorrgenes(5,:) = num2cell(genevar(idxuncorr));

outuncorr = [outpath '/uncorr_sig_corr_expr-dist.txt'];
uncorrgenes(2,:) = cellfun(@(x) num2str(x), uncorrgenes(2,:),'UniformOutput',false );
uncorrgenes(3,:) = cellfun(@(x) num2str(x), uncorrgenes(3,:),'UniformOutput',false );
uncorrgenes(4,:) = cellfun(@(x) num2str(x), uncorrgenes(4,:),'UniformOutput',false );
uncorrgenes(5,:) = cellfun(@(x) num2str(x), uncorrgenes(5,:),'UniformOutput',false );

fid = fopen(outuncorr,'w');
fprintf(fid,'%s %s %s %s %s \n', uncorrgenes{:});
fclose(fid);


% Non-Parametric significance test
% ================================
fprintf("> P-value of the results using non-parametric approach.  \n");
npermut = 1000;
diffpermut = zeros(npermut,size(modulesgeneprofile,1));
for idx0=1:npermut
    
    if mod(idx0,500)== 0 
        fprintf(">      Permutation number: %s  \n", num2str(idx0));
    end

    % Define randomoutput
    randntwkbasedcorr = zeros(1,size(modulesgeneprofile,1));
    
    % Permuting all connections    
    idxrand = randperm(length(datanet(:)),length(datanet(:)));
    randmat = datanet(idxrand);
    randmat = reshape(randmat,size(datanet,1), size(datanet,2));
    n = length(randmat);
    randmat(1:n+1:end) = 0;
    randmat = triu(randmat) + triu(randmat)';
    
    invrandnet = 1./randmat;
    invrandnet(randmat==0) = 0;
    randdist = distance_wei(invrandnet);
    cpermutdist = randdist(seedmodule,:);
    cpermutdist = [cpermutdist(1,1:seedmodule-1) cpermutdist(1,seedmodule+1:end)];
    for idx1=1:size(modulesgeneprofile,1)  % Loop over all genes
        % Compute correlation seed distance with gene expression
        gexp = modulesgeneprofile(idx1,:);
        cgexp = [gexp(1,1:seedmodule-1) gexp(1,seedmodule+1:end)];
        [cc,pp] = corr(cgexp(:),cpermutdist(:), 'Type', 'Spearman');
        randntwkbasedcorr(idx1) = cc;
    end
    % save permutation
    diffpermut(idx0,:) = randntwkbasedcorr;    
end

% Find significant genes
overcount = sum((diffpermut >= ntwkbasedcorr) , 1);
undercount = sum((diffpermut <= ntwkbasedcorr), 1);
pvalueover = overcount / npermut;
pvalueunder = undercount / npermut;
signthresh = round((0.005) * npermut); 
posover = find(overcount < signthresh);
posunder = find(undercount < signthresh);

outover = [outpath '/permut_corrected_over_corr_gExpr-seedDist.txt'];
outunder = [outpath '/permut_corrected_under_corr_gExpr-seedDist.txt'];
if isempty(posover) == 0
    signameover = geneid(posover);
    spvalueover = num2cell(pvalueover(posover));
    sdiff = ntwkbasedcorr(posover);
    cellover = [signameover(:), spvalueover(:), num2cell(sdiff(:))];
    
else
    signameover = {''};
    spvalueover = {''};
    sdiff = {''};
    cellover = [signameover, spvalueover, sdiff];
end
if isempty(posunder) == 0
    signameunder = geneid(posunder);
    spvalueunder = num2cell(pvalueunder(posunder));
    sdiff = ntwkbasedcorr(posunder);
    cellunder = [signameunder(:), spvalueunder(:), num2cell(sdiff(:))];
else
    signameunder = {''};
    spvalueunder = {''};
    sdiff = {''};
    cellunder = [signameunder, spvalueunder, sdiff];
end

% Sort cells by pvalue
cellover = sortrows(cellover,[2]);
cellunder = sortrows(cellunder,[2]);


cellover(1:end, 2) = cellfun(@(x) num2str(x), cellover(1:end, 2),'UniformOutput',false );
cellover(1:end, 3) = cellfun(@(x) num2str(x), cellover(1:end, 3),'UniformOutput',false );
cellunder(1:end, 2) = cellfun(@(x) num2str(x), cellunder(1:end, 2),'UniformOutput',false );
cellunder(1:end, 3) = cellfun(@(x) num2str(x), cellunder(1:end, 3),'UniformOutput',false );

% Save to txt file
fid = fopen(outover,'w');
overtranspose = cellover .';
fprintf(fid,'%s %s %s \n', overtranspose{:});
fclose(fid);
fid = fopen(outunder,'w');
undertranspose = cellunder .';
fprintf(fid,'%s %s %s \n', undertranspose{:});
fclose(fid);





#!/usr/bin/env python
# generate-surrogate-maps.py

#
# Given a list of subject, this script will compute an spatial auto-correlated
# surrogate map from ORIGINAL PET-tau volumes. This will be repeated N times
#
# Original Author: Victor Montal
# Date_first: Jan-28-2022
# Date:

# ---------------------------
# Import Libraries
from __future__ import print_function
import sys
import argparse
import os
from os.path import join

import numpy as np
import nibabel as nib

from brainsmash.workbench.geo import volume
from brainsmash.mapgen.sampled import Sampled
from brainsmash.mapgen.eval import sampled_fit

# ---------------------------
# Parser Options
HELPTEXT = """

generate-surrogate-maps.py [dev version]

Given a .txt list of subject images, this script will compute the spatial
auto-correlated surrogate volumes for each of them.
This will be repeated Nperm times.

Algorithm diagram:
------------------
1.- Load N-subj volume data
2.- Compute surrogate spatial auto-correlated map Nperm times
3.- Save Nperm .nii surrogate volumes
4.- Repeat for all N-subjects

INPUTS:
-------


Author:
------
Victor Montal
vmontalb [at] santpau [dot] cat

"""

USAGE = """

"""

def options_parser():
    """
    Command Line Options Parser:
    initiate the option parser and return the parsed object
    """
    parser = argparse.ArgumentParser(description = HELPTEXT,usage = HELPTEXT)

    # help text
    h_txtsubj   = '.txt file with subjects to subsample. one-by-row. Full-path'
    h_nperm     = 'Number of times the process will be repeated'
    h_mask      = 'Original mask that output the voxel coordinates'
    h_outpath   = 'Path where all subsets txt files will be placed.'


    parser.add_argument('--subj-list',  dest = 'subjectslist',  action = 'store',       help = h_txtsubj,   required = True)
    parser.add_argument('--permut',     dest = 'nperm',         action = 'store',       help = h_nperm,     required = True,  type = int)
    parser.add_argument('--mask',       dest = 'mask',          action = 'store',       help = h_mask,      required = True)
    parser.add_argument('--outpath',    dest = 'outpath',       action = 'store',       help = h_outpath,   required = True)

    args = parser.parse_args()
    return args

# ---------------------------
# Subfunctions
def my_print(message):
    """
    print message, then flush stdout
    """
    print(message)
    sys.stdout.flush()

# ---------------------------
# Main Code
def surrogateMaps(options):
    """
    0. Load original txt file of subjects data (full-path)
    1. Load voxel coordinates
    2. Load one-by-one volume data
    3. Compute surrogate image Nperm times.
    4. Save image
    5. Repeat for all subjects in list
    """
    # Priors
    kwargs = {'ns': 500,
          'knn': 500,     #This value is very low since we are working with huge voxels
          'pv': 60
          }

    print("> Loading image list")
    print(".         File: "+str(options.subjectslist))
    subjlist = np.loadtxt(options.subjectslist, dtype = object)
    print(".         Total number subjects: "+str(subjlist.shape[0]))

    print("\n> Loading mask")
    print(".         File: "+str(options.subjectslist))
    mask = nib.load(options.mask)
    fmask = mask.get_fdata()

    print("\n> Parsing voxel coordinates from mask")
    aff = mask.affine
    posmask = np.where(fmask == 1)
    voxcoord = np.zeros([posmask[0].size, 3])  # Matrix Nvox=1 by 3
    for cpos in range(posmask[0].size):
        cvox = [posmask[0][cpos], posmask[1][cpos], posmask[2][cpos], 1]
        ccoord = aff @ cvox  # matrix multiplication
        voxcoord[cpos,:] =  ccoord[0:3]


    print("\n> Computing distance matrix between voxels")
    tmpout = join(options.outpath,'Dist-matrix')
    if not os.path.exists(tmpout):
        os.makedirs(tmpout)
    # Save coord
    fcoord = join(tmpout,'voxel-coordinates.txt')
    np.savetxt(fcoord, voxcoord, fmt='%3d', delimiter=',')
    # Compute distance
    priors = volume(voxcoord,tmpout)

    print('\n> Generating output structure')
    print(".         Path: "+str(options.subjectslist))
    for idx in range(options.nperm):
        citer = "%03d" % (idx)
        tocreate = join(options.outpath,'permut'+citer,'surrogate-maps')
        if not os.path.exists(tocreate):
            os.makedirs(tocreate)

    print("> Starting to generate surrogate maps..")
    for idx,cvol in enumerate(subjlist):
        volname = os.path.basename(cvol)
        print(">  Working with volume "+str(idx+1)+": "+volname)

        # Load volume
        vol = nib.load(cvol)
        fvol = vol.get_fdata()

        # Get vector values inside mask
        volmasked = fvol[posmask]
        #sampled_fit(volmasked, priors['D'], priors['index'], nsurr=50, **kwargs)

        # Compute Nperm surrogates
        gen = Sampled(x=volmasked,
                        D=priors['D'],
                        index=priors['index'],
                        resample = True,
                        **kwargs)
        surrogate_maps = gen(n=options.nperm)

        # Save surrogates
        for idx2 in range(options.nperm):
            # prepare Output
            citer = "%03d" % (idx2)
            permpath = join(options.outpath,'permut'+citer,'surrogate-maps')
            tosave = join(permpath,'surrogate-'+volname)

            # Parse current surrogate
            csurrogate = np.zeros_like(fvol)               # init empty vol
            csurrogate[posmask] = surrogate_maps[idx2,:]   # fill mask values with surrogate
            csurrogatenii = nib.Nifti1Image(csurrogate, vol.affine)

            # Save .nii
            nib.save(csurrogatenii, tosave)




if __name__ == "__main__":
    options = options_parser()
    surrogateMaps(options)

%% Script to transform from MNI volume to Adjency matrix
% It must be a 4D nifti volume

clear;
clc;

invol = 'inputdir/raw_vols/HABS+ADNI_PIBpos_HC_rs8_tauPET.nii.gz';
maskvol = 'inputdir/Mask_creation/MNI_mask_GM_filtered_offtarget_8mm.nii.gz';
outfile = 'projectdir/conn-matrix/TAU-PET_HABS+ADNI_Abpos_cross_adjency_rs8.mat';


%Read mask
hdr = spm_vol(maskvol);
hdrmask = spm_read_vols(hdr);
rshdr = reshape(hdrmask,[],1);
shapemask = size(hdrmask); % b/c matlab starts at 1 no zero
idx_mask = find(rshdr > 0);
%mnizeros = zeros(size(hdrmask));
mnicoord = zeros(length(idx_mask),3);
postocoord = hdr.mat;

for idx1=1:length(idx_mask)
    posz = floor(idx_mask(idx1)./(shapemask(2) * shapemask(1)));
    posy = floor( (idx_mask(idx1) - (posz * shapemask(2) * shapemask(1)))./ shapemask(1));
    posx = idx_mask(idx1) - posz * shapemask(2) * shapemask(1) - posy * shapemask(1);
    coord = postocoord * [posx+1 posy+1 posz+1 1]' ;  
    mnicoord(idx1,:) = coord(1:3); 
    % mnizeros(uint8(posx),uint8(posy+1),uint8(posz+1)) = 1;
end

num_rois=length(idx_mask);


%Read 4D volume
[hdr]=spm_vol(invol);
hdrvol = spm_read_vols(hdr);
rshdrvol=reshape(hdrvol,[],size(hdrvol,4));
rshdrvol=rshdrvol(idx_mask,:);

% Compute adjency matrix
[netpet,pvals]=corr(rshdrvol');
netpet(1:(num_rois+1):end)=0; %diagonal to zero
netpet(isnan(netpet)) = 0;

% Save outputs
save(outfile,'netpet', 'mnicoord');

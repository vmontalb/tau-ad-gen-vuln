#!/usr/bin/env python3
# gmm_filter_offtarget_tau.py
# -*- coding: utf-8 -*-
##########################
# Script to filter off-target bining of TAU PET using GMM
#########################


# ----------------------------------------------
# Import Libraries
from __future__ import print_function
import argparse
from argparse import RawTextHelpFormatter
import os
import shutil
from os.path import join
import subprocess
import sys
import time

import numpy as np
import nibabel as nib
from sklearn.mixture import GaussianMixture

import pdb



# ----------------------------------------------
# - Parser input options
def options_parse():
    """
    Command Line Options Parser:
    initiate the option parser and return the parsed object
    """
    HELPTEXT = '''

    gmm_filter_offtarget_tau.py [dev version]

    Summary
    -------
    > Script to filter off-target bining of TAU PET using GMM
    > It will fita 1-component and a 2-component GMM for each voxelself.
      It will compute and compare the AIC value for each model.
      If the 2-component overperformed the 1-component, it means its not offtarget


    Cite: Vogel et al 2019; Spread of pathological tau proteins through communicating neurons in human Alzheimer’s disease


    Inputs
    ------
    mni-mask: Path to MNI GM mask.


    tauvols: Path to 4D nifti file with all subjects concatenated (e.g 4D_HC_justCross_TauPet.nii.gz)
             Same resolution as mni-mask

    outdir: Path where resulting mask will be saved


    Author:
    ------
    Victor Montal
    vmontalb [at] santpau [dot] cat

    '''

    USAGE = """

    """

    parser = argparse.ArgumentParser(description = HELPTEXT,formatter_class=RawTextHelpFormatter)
    # help text
    h_subjlist  = 'Txt file with the Subjects to be processed'
    h_fsdir     = 'Path to Freesurfers processed subjects'
    h_outdir    = 'Path where resulting corrected volume will be saved'

    parser.add_argument('--mni-mask',    dest='mnimask',     help=h_subjlist,  required = True)
    parser.add_argument('--tauvols',    dest='tauvols',     help=h_fsdir,     required = True)
    parser.add_argument('--outdir' ,    dest='outdir',      help=h_outdir,    required = True)


    args = parser.parse_args()

    # Check options exists
    return args


# - Supplementary functions
def my_print(message):
    """
    print message, then flush stdout
    """
    print(message)
    sys.stdout.flush()

def run_cmd(cmd,err_msg):
    """
    execute the comand
    """
    my_print('#@# Command: ' + cmd+'\n', ff)
    retcode = subprocess.Popen(cmd,shell=True, executable='/bin/bash').wait()
    if retcode != 0 :
        my_print('ERROR: '+err_msg, ff)
        sys.exit(1)
    my_print('\n', ff)

# - Main function
def offbinding_TAU(options):
    """
    1) Load MNI mask
    2) Load 4D volume
    3) Check they share resolution
    4) Just focus on mask voxels
    5) For each voxel
        5.1) Compute 1-compartiment GMM model and their AIC
        5.2) Compute 2-compartiment GMM model and their AIC
    6) Exclude from the final mask those voxels were AIC_1comp > AIC_2comp
    7) Save final mask
    """

    # Load volumes
    my_print("> Loading MNI mask")
    mnivol = nib.load(options.mnimask)
    mnimask = mnivol.get_data()
    rsmask = mnimask.reshape(mnimask.shape[0]*mnimask.shape[1]*mnimask.shape[2], -1)
    my_print("> Loading 4D TAU PET volumes")
    tauorig = nib.load(options.tauvols)
    tauvols = tauorig.get_data()
    rstau = tauvols.reshape(tauvols.shape[0]*tauvols.shape[1]*tauvols.shape[2], -1)

    if rstau.shape[0] != rsmask.shape[0]:
        my_print("Whoa! MNI mask and 4D volume differs in dimensions. Double check files and run again.")
        sys.exit(1)

    # Compute GMM for each voxel
    my_print("> Assesing offtarget binding of each voxel (using 1/2-GMM compartiments)")
    # Positions mask
    posmask = np.where(rsmask == 1)[0]
    # Define output AIC volume and mask
    aicvol = np.zeros_like(rsmask)
    # Loop over all voxels in GM mask
    for idx,pos in enumerate(posmask):
        currdat = rstau[pos,:]
        currdat = currdat.reshape(-1,1)
        onecomp = GaussianMixture(1).fit(currdat).aic(currdat)
        twocomp = GaussianMixture(2).fit(currdat).aic(currdat)
        if twocomp < onecomp:
            aicvol[pos] = 2
        else:
            aicvol[pos] = 1
        my_print(". AIC diff: "+str(twocomp - onecomp))

    # Updating mask
    my_print("> Updating final mask and saving output files")
    rsoutmask = np.zeros_like(rsmask)
    rsaic = np.zeros_like(rsmask)

    pdb.set_trace()
    rsoutmask[posmask] = aicvol[posmask] - 1
    rsaic[posmask] = aicvol[posmask]
    outmask = rsoutmask.reshape(mnimask.shape[0], mnimask.shape[1], mnimask.shape[2])
    outaic = rsaic.reshape(mnimask.shape[0], mnimask.shape[1], mnimask.shape[2])

    outnamemask = join(options.outdir, "MNI_mask_GM_filtered_offtarget_8mm.nii.gz")
    outnameaic = join(options.outdir, "MNI_AIC_GMM_offtarget_8mm.nii.gz")

    outimgmask = nib.Nifti1Image(outmask, mnivol.affine)
    outimgaic = nib.Nifti1Image(outaic, mnivol.affine)
    nib.save(outimgmask, outnamemask)
    nib.save(outimgaic, outnameaic)




# - Run code
if __name__=="__main__":
    # Command Line options and error checking done here
    options = options_parse()
    offbinding_TAU(options)
